'use strict';

/**
 * search-place service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::search-place.search-place');
