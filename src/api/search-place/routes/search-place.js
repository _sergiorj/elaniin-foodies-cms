'use strict';

/**
 * search-place router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::search-place.search-place');
