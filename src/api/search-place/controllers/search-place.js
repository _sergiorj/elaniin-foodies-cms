'use strict';

/**
 * search-place controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::search-place.search-place');
