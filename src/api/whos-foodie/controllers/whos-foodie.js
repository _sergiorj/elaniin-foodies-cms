'use strict';

/**
 * whos-foodie controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::whos-foodie.whos-foodie');
