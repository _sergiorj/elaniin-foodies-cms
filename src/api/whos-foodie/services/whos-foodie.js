'use strict';

/**
 * whos-foodie service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::whos-foodie.whos-foodie');
