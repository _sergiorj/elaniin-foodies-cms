'use strict';

/**
 * whos-foodie router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::whos-foodie.whos-foodie');
