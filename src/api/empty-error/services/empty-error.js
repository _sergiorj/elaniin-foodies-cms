'use strict';

/**
 * empty-error service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::empty-error.empty-error');
