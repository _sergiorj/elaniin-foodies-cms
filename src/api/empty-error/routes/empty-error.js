'use strict';

/**
 * empty-error router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::empty-error.empty-error');
