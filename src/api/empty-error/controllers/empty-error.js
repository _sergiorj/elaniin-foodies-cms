'use strict';

/**
 * empty-error controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::empty-error.empty-error');
