# Foodies CMS

Version 🏷️ 1.0.0

## Getting Started

### First, add node packages 📦.

```bash
yarn
# or
npm install
```

### Second, create .env file in root `/.env` and add variables 🔐.

### Third, import schemas 🌐.

- Add the resource file to `./backup/` and then follow the next command.

```bash
yarn strapi import -f ./backup/[file-name].tar.gz
# or
npm strapi import -f ./backup/[file-name].tar.gz
```

- If you want to export shemas follow the nex command.

```bash
 yarn strapi export --file ./backup/[file-name] --no-encrypt
# or
 npm strapi export --file ./backup/[file-name] --no-encrypt
```

### Fourth, run the development server 💻.

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:1337](http://localhost:1337) with your browser to see the login and [http://localhost:1337/admin](http://localhost:1337/admin) to manage your server.

### Add DataBase to PGAdmin 🐘.

Use the data in .env file to fill the inputs.

- Step 1: Open PGAdmin 4

![step-one](https://res.cloudinary.com/dqluhb4wx/image/upload/v1674237493/Steps/step-1_r9a11t.png)

Step 2: Use your own credentials to access to PGAdmin 4
![step-two](https://res.cloudinary.com/dqluhb4wx/image/upload/v1674237485/Steps/step-2_seq3yy.png)

Step 3: Create a new server
![alt text](https://res.cloudinary.com/dqluhb4wx/image/upload/v1674237486/Steps/step-3_udxazf.png)

Step 4: Fill the inputs with .env data
![alt text](https://res.cloudinary.com/dqluhb4wx/image/upload/v1674237486/Steps/step-4_vtqyt6.png)

Step 5: Fill the inputs with .env data
![alt text](https://res.cloudinary.com/dqluhb4wx/image/upload/v1674237486/Steps/step-5_namqnl.png)

Step 6: Click save, now you can watch all the data in the db.
